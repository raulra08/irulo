# iRulo

iOS Blank App for CI/CD

# CI/CD

## Setting Up New CI/CD for iOS Development

### Requirements

Create a GitLab repo

Setup a GitLab runner - https://about.gitlab.com/blog/2016/03/10/setting-up-gitlab-ci-for-ios-projects/

```
sudo curl --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-darwin-amd64\n
sudo chmod +x /usr/local/bin/gitlab-runner\n
gitlab-runner register
gitlab-runner install
gitlab-runner start
```

### Testing

Run these two commands as part of your pipeline testing stage.

```
xcodebuild clean -project iRulo.xcodeproj -scheme iRulo | xcpretty
xcodebuild test -project iRulo.xcodeproj -scheme iRulo -destination 'platform=iOS Simulator,name=iPhone 11 Pro Max,OS=13.3' | xcpretty -s
```

### Deploying

Deploying involves creating an archive and an .ipa file.

```
xcodebuild -scheme iRulo -project iRulo.xcodeproj -sdk iphoneos -configuration Release clean archive -archivePath ./build/iRulo.xcarchive
xcodebuild -exportArchive -archivePath build/iRulo.xcarchive -exportPath build/ -exportOptionsPlist ExportOptions.plist
```
